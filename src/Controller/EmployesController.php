<?php

namespace App\Controller;

use App\Entity\Employes;
use App\Form\RegistrationFormType;
use App\Repository\EmployesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EmployesController extends AbstractController
{
    /**
     * @Route("/", name="employes")
     */
    public function index()
    {
        $employes = $this->getDoctrine()->getRepository(Employes::class)->findAll();
        return $this->render('employes/index.html.twig', [
            'employes' => $employes
        ]);
    }

    /**
     * @Route("delete/{employe}", name="delete_employes")
     */
    public function delete(Employes $employe)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($employe);
        $manager->flush();

        return $this->redirectToRoute('employes');
    }

    /**
     * @Route("add/employe", name="add_employe")
     */
    public function addEmploye(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new Employes();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $imageFile = $form->get('image')->getData();

            if ($imageFile) {
                $newFilename = uniqid() . '.' . $imageFile->guessExtension();
                try {
                    $imageFile->move(
                        $this->getParameter('avatar_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    die(dump($e));
                }

                $user->setImage('/images/employes/' . $newFilename);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('employes');
        }
        return $this->render('employes/add.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

}
