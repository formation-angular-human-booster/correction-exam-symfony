<?php
// src/Validator/Constraints/ContainsAlphanumeric.php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DomainEmail extends Constraint
{
    public $message = 'Le domaine ne correspond pas à celui de la société.';

}