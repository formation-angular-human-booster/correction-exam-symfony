<?php
// src/Validator/Constraints/ContainsAlphanumericValidator.php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DomainEmailValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $arrMail = explode('@', $value);
        if ($arrMail[1] != 'deloitte.com') {
            // the argument must be a string or an object implementing __toString()
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}
?>