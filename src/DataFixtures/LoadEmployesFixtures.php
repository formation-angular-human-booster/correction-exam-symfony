<?php
// src/DataFixtures/AppFixtures.php
namespace App\DataFixtures;

use App\Entity\Employes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class LoadEmployesFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new Employes();
        $user->setFirstname('Admin');
        $user->setLastname('Admin');
        $user->setEmail('admin@deloitte.com');
        $user->setImage('/images/employes/avatar.png');
        $user->setSecteur('Direction');
        $user->setRoles(['ADMIN']);
        $password = $this->encoder->encodePassword($user, 'admin123@');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();

        $user = new Employes();
        $user->setLastname('Secretaire');
        $user->setFirstname('Secretaire');
        $user->setEmail('secretaire@deloitte.com');
        $user->setImage('/images/employes/avatar.png');
        $user->setSecteur('Direction');
        $user->setRoles(['ADMIN']);
        $password = $this->encoder->encodePassword($user, 'secretaire123@');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }
}
?>